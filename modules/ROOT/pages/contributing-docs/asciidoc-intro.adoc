= AsciiDoc for Fedora Documentation Writers
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
v0.0.1, 2022-09-26

This documentation section provides advice, tips, and resources for the Fedora Community writing AsciiDoc for Fedora documentation.
The purpose of this section is not to duplicate what already exists, but to point you the right way.
The idea is for you to spend less time remembering AsciiDoc or Antora nitpicks, and more time writing good docs!

_Use the navigation bar in the side to navigate this section._
