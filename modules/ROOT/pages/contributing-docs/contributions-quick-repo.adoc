= How to contribute to Quick Docs repo
Hanku Lee, Peter Boy; Fedora Documentation Team
:revdate: 2023-03-10

[abstract]
Fedora Pagure is an Open Source software code hosting system that supports various Fedora project repositories such as, but not limited to, Quick Docs, Server, EPEL (Extra Packages for Enterprise Linux), Modularity and Packaging Committee. This article steps through contribution process for Quick Docs, which contain how-to guides and tutorials for the Fedora Linux. The overall process can be translated for any other Pagure projects. Pre-requisites to begin your first contribution are Fedora account ID, signed contributor agreement, and some familiarity with git pull request. Docs team is looking for more folks from the community helping with docs.

== How to find tasks

Whether it is broken link, outdated screenshots of graphical user interface, technical and grammatical error in Quick Docs, every little contribution helps Fedora Linux users.

The best place to find how to contribute to Quick Docs is issue board where users report documentation bugs or suggestions to improve documentation.

Track issues on the link: https://pagure.io/fedora-docs/quick-docs/issues

=== Self-assign issues

If you find issue that matches your interest and expertise, on assignee on top right, press *take* link to assign issue to yourself.

.Assign issues
image::pagure-guide/assign-issues.png[]

== Rule sets

Beyond style guide, here are a few rule sets contributors need to observe.

- The process Docs team treat bugs in documentation follows the same principle as code bugs where continuous integration, automated testing, and gradual improvements for documentation quality are adopted.

- All changes to Quick Docs must be done via Pull Requests from forked project. Quick Docs project does not support direct push to its upstream repo.

- If you fix contents about new features, releases or new content, test the application and process in a new system or freshly installed Virtual Machine with all updates applied.

- Do not use screenshot of terminal. Provide source code block of a particular language or shell commands.

- Run prose linter like vale locally before PR. Check this link: https://docs.fedoraproject.org/en-US/fedora-docs/contributing-docs/tools-vale-linter/

== Pull Requests and review

Pull Requests promote collaborative work flow between writers and reviewers where PR comments, edits and commits serve as review process.

=== Write changes and push to Quick Docs repo

Please find general Pull Requests (PR) work flow. See the link: https://docs.fedoraproject.org/en-US/fedora-docs/contributing-docs/tools-local-authoring-env/

=== Create a pull request from Pagure User Interface

- Go to your forked project and click *View Fork* on top right of Quick Docs project landing page: https://pagure.io/fedora-docs/quick-docs

- On your fork, click Open PR and New Pull Request

- Locate your feature branch (pull from) next to your fork, under main branch

- Fill in Pull Request Title and describe your changes

- Check files changed and commits before Pull Request

- Press the Create Pull Request button below on the right

=== Notification after PR

Watch status in the middle of Quick Doc report has four options. Select one of the options from below.

- Watch Issues and PRs

- Watch Commits

- Watch Issues, PRs, and Commits

- Unwatch

Notification will go to your email as in user settings of Pagure.

.Notification options
image::pagure-guide/watch-notification.png[]

If you do not hear from reviewers after your PR longer than a week, leave a comment in PR to follow up.

== Docs team needs your help

Help us with the issues, PRs, onboarding new contributors or keeping our stock of articles as a whole up to date. Thank you!
